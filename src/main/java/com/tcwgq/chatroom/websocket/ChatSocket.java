package com.tcwgq.chatroom.websocket;

import com.alibaba.fastjson2.JSON;
import com.alibaba.fastjson2.TypeReference;
import com.tcwgq.chatroom.util.MessageUtil;

import javax.servlet.http.HttpSession;
import javax.websocket.*;
import javax.websocket.server.ServerEndpoint;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * websocket实现方式二：使用注解
 *
 * @author tcwgq
 * @since 2022/7/28 16:34
 */
@ServerEndpoint(value = "/websocket", configurator = HttpSessionConfigurator.class)
public class ChatSocket {
    /**
     * 每个用户建立的聊天映射
     */
    private static final Map<HttpSession, ChatSocket> onlineUsers = new HashMap<>();
    private static int onlineCount = 0;
    /**
     * 当前建立的websocket session
     */
    private Session session;
    /**
     * 当前建立的httpSession
     */
    private HttpSession httpSession;

    @OnOpen
    public void onOpen(Session session, EndpointConfig config) {
        // 1. 记录webSocket的会话信息对象Session
        this.session = session;

        // 2. 获取当前登录用户HttpSession信息.
        this.httpSession = (HttpSession) config.getUserProperties().get(HttpSession.class.getName());

        System.out.println("当前登录用户 : " + httpSession.getAttribute("username") + ", Endpoint : " + hashCode());

        // 3. 记录当前登录用户信息, 及对应的Endpoint实例
        Object username = httpSession.getAttribute("username");
        if (username != null) {
            onlineUsers.put(httpSession, this);
        }

        // 4. 获取当前所有登录用户，格式：ITCAST,HEIMA,TOM...
        String names = getNames();

        // 5. 组装消息，格式：{"data":"HEIMA,Deng,ITCAST","toName":"","fromName":"","type":"user"}
        String content = MessageUtil.getContent(MessageUtil.TYPE_USER, "", "", names);

        // 6. 通过广播的形式发送消息
        // session.getBasicRemote().sendText("");
        broadcastAllUsers(content);

        // 7. 记录当前用户登录数
        increase();
    }

    @OnMessage
    public void onMessage(String message, Session session) {
        System.out.println("onMessage : name = " + httpSession.getAttribute("username") + ", message=" + message);
        // 1. 获取客户端的信息内容, 并解析
        Map<String, String> messageMap = JSON.parseObject(message, new TypeReference<Map<String, String>>() {
        });
        String fromName = messageMap.get("fromName");
        String toName = messageMap.get("toName");
        String content = messageMap.get("content");

        // 2. 判定是否有接收人
        if (toName == null || toName.isEmpty()) {
            return;
        }
        // 3. 如果接收人是否是广播(all), 如果是, 则说明发送广播消息
        String messageContent = MessageUtil.getContent(MessageUtil.TYPE_MESSAGE, fromName, toName, content);
        System.out.println("服务端给客户端发送消息, 消息内容: " + messageContent);
        if ("all".equals(toName)) {
            // 3.1 组装消息内容
            broadcastAllUsers(messageContent);
        } else {
            // 4. 不是all , 则给指定的用户推送消息
            singlePushMessage(messageContent, fromName, toName);
        }
    }

    @OnClose
    public void onClose(Session session, CloseReason closeReason) {
        decrease();
        System.out.println("客户端关闭了一个连接 , 当前在线人数 : " + getOnlineCount());
    }

    @OnError
    public void onError(Session session, Throwable throwable) {
        throwable.printStackTrace();
        System.out.println("服务异常");
    }

    // 给指定用户推送消息
    private void singlePushMessage(String content, String fromName, String toName) {
        boolean isOnline = false;
        // 1. 判定当然接收人是否在线
        for (HttpSession httpSession : onlineUsers.keySet()) {
            if (toName.equals(httpSession.getAttribute("username"))) {
                isOnline = true;
            }
        }

        // 2. 如果存在, 发送消息
        if (isOnline) {
            for (HttpSession httpSession : onlineUsers.keySet()) {
                // 发送界面右边显示发送人消息，接收人左边显示消息
                if (httpSession.getAttribute("username").equals(fromName) ||
                        httpSession.getAttribute("username").equals(toName)) {
                    try {
                        onlineUsers.get(httpSession).session.getBasicRemote().sendText(content);
                    } catch (IOException e) {
                        throw new RuntimeException(e);
                    }
                }
            }
        }
    }

    /**
     * 发送广播消息
     *
     * @param content
     */
    private void broadcastAllUsers(String content) {
        onlineUsers.forEach((httpSession, chatSocket) -> {
            try {
                chatSocket.session.getBasicRemote().sendText(content);
            } catch (IOException e) {
                throw new RuntimeException(e);
            }
        });
    }

    public synchronized void increase() {
        onlineCount++;
    }

    public synchronized void decrease() {
        onlineCount--;
    }

    public int getOnlineCount() {
        return onlineCount;
    }

    /**
     * 获取所有在线用户
     *
     * @return
     */
    private String getNames() {
        if (onlineUsers.size() > 0) {
            return onlineUsers.keySet().stream()
                    .map(httpSession -> httpSession.getAttribute("username").toString())
                    .collect(Collectors.joining(","));
        }
        return "";
    }

}
