package com.tcwgq.chatroom.websocket;

import javax.websocket.Endpoint;
import javax.websocket.EndpointConfig;
import javax.websocket.Session;

/**
 * websocket实现方式一：继承Endpoint
 *
 * @author tcwgq
 * @since 2022/7/28 16:34
 */
public class MyWebSocket extends Endpoint {
    @Override
    public void onOpen(Session session, EndpointConfig config) {

    }

}
